#!/usr/bin/env python3
from db import DB

def test_db():
  db = DB('test')
  db._DB__remove_db()
  db._DB__connect_db()
  db._DB__create_table()
  db.insert_part({'name': 'bogus', 'count': 2})
  db.insert_part({'name': 'ULN2003A', 'count': 5})
  db.insert_part({'name': 'DS18B20', 'count': 3})
  db.increment_part_count(1, 3)
  db.update_part(2, {'type': 'Motor Driver'})
  db.update_part(3, {'type': 'Temperature Sensor'})

  for part in db.get_parts():
    print([part[key] for key in part.keys()])

  db.remove_part(1)
   
  for part in db.get_parts():
    print([part[key] for key in part.keys()])


if __name__ == '__main__':
  test_db()
