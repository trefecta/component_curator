#!/usr/bin/env python3

import sqlite3
from pathlib import Path
from collections import OrderedDict

FIELDNAMES = {
  'name': "TEXT",
  "type": "TEXT",
  'datasheet_uri': "TEXT",
  'manufacturer': "TEXT",
  'count': "INTEGER",
  'cost': "INTEGER",
  'project': "TEXT"
}

FIELD_TMPL_STR = ', '.join([name + " " + sqltype for name, sqltype in FIELDNAMES.items()])
FIELD_LIST_STR = ', '.join([field for field in FIELDNAMES.keys()])
FIELD_REPL_STR = ', '.join([':' + field for field in FIELDNAMES.keys()])
FIELD_MAP_STR  =', '.join([field + '=:' + field for field in FIELDNAMES])

statements = {
  'create': '''
     CREATE TABLE parts (id INTEGER PRIMARY KEY, {})
  '''.format(FIELD_TMPL_STR),
  'get': '''
    SELECT * FROM parts WHERE id=:id
  ''',
  'get_all': '''
    SELECT * FROM parts
  ''',
  'insert': '''
     INSERT INTO parts({}) VALUES ({})
  '''.format(FIELD_LIST_STR, FIELD_REPL_STR),
  'update': '''
    UPDATE parts SET {} WHERE id=:id
  '''.format(FIELD_MAP_STR),
  'update_count': '''
    UPDATE parts SET count=:count WHERE id=:id
  ''',
  'search': '''
    SELECT * FROM parts WHERE name LIKE %:name%
  ''',
  'remove': '''
    DELETE FROM parts WHERE id=:id
  '''
}

class DB(object):
  """ Control Parts Table in Database """

  def __init__(self, db_name, replace=False):

    self.db_name = db_name
    self.db_path = Path(__file__).resolve().parents[1] / 'data' / (self.db_name + '.sqlitedb')
    self.conn = None
    self.cursor = None

    if replace:
      self.__remove_db()

    if not self.db_path.parent.exists():
      # Initialize new file
      try:
        self.db_path.parent.mkdir()
      except FileExistsError as e:
        pass # ignore making directory if it already exists

    self.__connect_db()
    self.__create_table()

  
  def __format_data_fields(self, data, existing_only=False):
    if existing_only:
      return OrderedDict([(field, data.get(field)) for field in FIELDNAMES if field in data])
    
    return OrderedDict([(field, data.get(field)) for field in FIELDNAMES])

  def __run_statement(self, statement_name, data=None):
    if statement_name not in statements:
      return sqlite3.Warning('Statement "{}" not available.'.format(statement_name))

    statement = statements.get(statement_name)

    try:
      if data:
        self.cursor.execute(statement, data)
      else:
        self.cursor.execute(statement)
    except sqlite3.Error as e:
      raise(e)
    else:
      self.conn.commit()

    return self.cursor

  def get_parts(self):
    self.cursor.execute(statements.get('get_all'))
    return self.cursor.fetchall()


  def get_part(self, part_id):
    self.__run_statement('get', {'id': part_id})
    return self.cursor.fetchone()


  def search_parts(self, part_string):
    self.__run_statement('search', {'name': part_string})
    return self.cursor.fetchall()


  def insert_part(self, part_data):
    data = self.__format_data_fields(part_data)
    self.__run_statement('insert', data)
    return self.cursor.fetchone()


  def update_part(self, part_id, part_data):
    old_data = self.__run_statement('get', {'id': part_id}).fetchone()

    data = {key: old_data[key] for key in old_data.keys()}
    data.update(self.__format_data_fields(part_data, True))

    self.__run_statement('update', data)

    return self.cursor.fetchone()


  def update_part_count(self, part_id, count):
    if count < 0:
      return sqlite3.Warning('Part count cannot be negative.')
    
    data = {'id': part_id, 'count': count}

    self.__run_statement('update_count', data)

    return self.cursor.fetchone()


  def increment_part_count(self, part_id, increment_by=1):

    try:
      count = self.__run_statement('get', {'id': part_id}).fetchone()['count']
    except Exception as e:
      raise(e)
    else:
      count += increment_by
  
    return self.update_part_count(part_id, count)

  def remove_part(self, part_id):
    return self.__run_statement('remove', {'id': part_id}).fetchone()

  def __create_table(self):
    return self.__run_statement('create')

  def __connect_db(self):
    if not self.db_path.exists():
      try:
        self.db_path.touch(exist_ok=False)
      except FileExistsError as e:
        print('Database file {:s} already exists. Skipping creation.'.format(self.db_name))
      else:
        print('INFO:', 'Database "' + self.db_name + '" created.')

    # Get database connection
    try:
      self.conn = sqlite3.connect(str(self.db_path))
    except sqlite.DatabaseError as e:
      print('INFO: Could not connect to database "' + self.db_name + '".')
      raise(e)
    else:
      print('INFO: Connected to database "' + self.db_name + '".')
    self.conn.row_factory = sqlite3.Row
    self.cursor = self.conn.cursor()
    
    return self.conn

  def __remove_db(self):
    self.close()
    try:
      self.db_path.unlink()
    except Exception as e:
      raise(e)
    else:
      print('INFO: Database "' + str(self.db_name) + '" removed.')
    return True

  def close(self):
    try:
      self.conn.close()
    except:
      print('INFO: No connection to close.')
