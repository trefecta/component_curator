# Electronic Component Curation Tool

Application for organization and curation of electronic components.

## TODO

* Add real tests
* Use argparse for command-line interface
* Constrain input data
* Format user input
* Allow for reference of parts by name (currently only by database assigned id)
* Metadata-referenced datasheet repository
* GUI
